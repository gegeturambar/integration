// Custom choices
$(document).ready(function(e) {
    CustomChoices();
});
$(document).ajaxComplete(function(e) {
    CustomChoices();
});
$(document).on('CustomForm', function(e) {
    CustomChoices();
});

function CustomChoices()
{
    if($('INPUT[type="checkbox"], INPUT[type="radio"]').length)
    {
        $('INPUT[type="checkbox"], INPUT[type="radio"]').each(function(){
            if($(this).parent().is('LABEL.custom_'+$(this).attr('type')) === false)
            {
                $(this).wrap('<label for="'+$(this).attr('id')+'" class="custom_'+$(this).attr('type')+'"></label>');
                $(this).after('<span class="crud_radio_check_selector"></span>');

                if($(this).is(':checked'))
                    $(this).parent().addClass('checked_'+$(this).attr('type'));

                if($(this).prop('disabled'))
                    $(this).closest('.custom_choice').addClass('disabled');

                if($(this).prop('readonly'))
                    $(this).closest('.custom_choice').addClass('readonly');
            }
        });

        $('.form_row .custom_choice + .custom_choice').each(function(){
            if($(this).parent().hasClass('form_row') && !$(this).parent().hasClass('custom_choice_w')) {
                $(this).parent().find('.custom_choice').wrapAll('<div class="custom_choice_w"></div>');
            }
        });
    }
}

$(document).on('change','INPUT[type="checkbox"], INPUT[type="radio"]',function(e){
    var input = $(this);

    if(input.is('INPUT[type="checkbox"]')) {
        if(input.is(':checked'))
            input.parent().addClass('checked_'+$(this).attr('type'));
        else
            input.parent().removeClass('checked_'+$(this).attr('type'));
    }
    else {
        input.closest('.form_row').find('[name="' + input.attr('name') + '"]').each(function(){
            if($(this).is(':checked'))
                $(this).parent().addClass('checked_'+$(this).attr('type'));
            else
                $(this).parent().removeClass('checked_'+$(this).attr('type'));
        });
    }
});

$(document).on('focus','INPUT[type="checkbox"], INPUT[type="radio"]',function(e){
    $(this).parent().addClass('focus');
});

$(document).on('blur','INPUT[type="checkbox"], INPUT[type="radio"]',function(e){
    $(this).parent().removeClass('focus');
});

$(document).on('click','.custom_choice LABEL',function(e){
    $(this).closest('.custom_choice').find('[id="' + $(this).attr('for') + '"]').blur();
});