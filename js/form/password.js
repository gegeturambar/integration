// Custom choices
$(document).ready(function(e) {
    CustomPassword();
});
$(document).ajaxComplete(function(e) {
    CustomPassword();
});
$(document).on('CustomForm', function(e) {
    CustomPassword();
});

function CustomPassword()
{
    $('INPUT[type="password"]').each(function(){
        if($(this).parent().is('DIV.password_w') === false)
        {
            $(this).wrap('<div class="password_w"></div>');
            $(this).before('<span class="icone crud-view"></span>');
            if($(this).val().length < 1)
                $(this).parent().find('.icone').css('display','none');
        }
    });
}

$(document).on('input','.password_w INPUT', function(){
    $(this).parent().find('.icone').css('display',$(this).val().length < 1 ? 'none' : '');
});

$(document).on('mousedown','.password_w .icone', function(){
    $(this).parent().find('INPUT').attr('type','text');
});

$(document).on('mouseup mouseover','.password_w .icone', function(){
    $(this).parent().find('INPUT').attr('type','password');
});