import {animation_speed, animation_ease} from '../var.js';

$(document).ready(function (e) {
    CustomErrorCKEditor();
    ScrollToError();
});

$(document).ajaxComplete(function (e) {
    CustomErrorCKEditor();
});

$(document).on('CustomForm', function (e) {
    CustomErrorCKEditor();
});

function CustomErrorCKEditor() {
    if (typeof(CKEDITOR) !== 'undefined' && CKEDITOR) {
        for (var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].on('change', function(e) {
                removeError($('#' + e.editor.name))
            });
        }
    }
}

$(document).on('change', 'input,select,textarea', function () {
    removeError($(this))
});

function removeError(that) {
    var form_row = that.closest('.form_row');
    var errorDisplay = form_row.find('.form_error_display');
    if(errorDisplay.length) {
        form_row.removeClass('form_error');
        errorDisplay.slideUp(animation_speed, animation_ease);
    }
}

function ScrollToError() {
    var error_crud = $('.content_w .content FORM.form_crud .form_row.form_error');
    if(error_crud.length) {
        $('html, body').animate({
            scrollTop: error_crud.first().offset().top - $('.header').outerHeight(),
        }, animation_speed, animation_ease);
    }
}