// Custom input
$(document).ready(function(e) {
    CustomInput();
    CustomCopyOnly();
});
$(document).ajaxComplete(function(e) {
    CustomInput();
    CustomCopyOnly();
});
$(document).on('CustomForm', function(e) {
    CustomInput();
    CustomCopyOnly();
});


function CustomInput()
{
    $('INPUT[icon]').each(function(){
        if($(this).parent().is('DIV.input_w') === false)
        {
            $(this).wrap('<div class="input_w"></div>');
            $(this).before('<span class="icone ' + $(this).attr('icon') + '"></span>');
        }
    });
}

function CustomCopyOnly()
{
    $('INPUT[copyonly],TEXTAREA[copyonly]').each(function(){
        if($(this).not('[readonly]'))
        {
            $(this).prop('readonly',true);
            $(this).click(function(){
                $(this).select();
            });
            $(this).focus(function(){
                $(this).select();
            });
        }
    });
}