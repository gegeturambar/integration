import {animation_speed, animation_ease} from '../var.js';

$(document).ready(function (e) {
    clearInterval(intervalPrepareIframeModal);
    prepare_iframe_modal();
});
$(document).ajaxComplete(function (e) {
    prepare_iframe_modal();
});
function prepare_iframe_modal() {
    $('.modal_crud_w').each(function(){
        var modal = $(this);
        modal.find('IFRAME').each(function(){
            var iframe = $(this);
            if(iframe.is('[src]')) {
                iframe.data('src',iframe.attr('src'));
                iframe.removeAttr('src');
            }
        });
        if(!modal.parent().is('BODY') && modal.is('[data-modal^="dnl_media_video_"]'))
            modal.appendTo('BODY');
    });
}
var intervalPrepareIframeModal = setInterval(function(){prepare_iframe_modal();}, 50);

$(document).on('click','[data-button-modal]',function(e){
    
    var that = $(this);
    var modal = $('.modal_crud_w[data-modal="' + that.data('button-modal') + '"]');
    
    $('BODY').addClass('modal_open');
    modal.css('opacity','0');
    modal.css('display','block');
    modal.find('IFRAME').each(function(){
        var iframe = $(this);
        iframe.attr('src',iframe.data('src'));
        iframe.removeData('src');
    });
    resize_modal();
    modal.css('opacity','');
    modal.css('display','');
    modal.fadeIn(animation_speed, animation_ease);
})

$(document).on('click','.modal_crud_w, .modal_crud_w .modal_crud_close_trigger',function(e){
    
    var that = $(e.target);
    var modal = false;
    
    if(that.hasClass('modal_crud_close') || that.hasClass('modal_crud_close_trigger'))
        modal = that.closest('.modal_crud_w').data('modal');
    else if(that.hasClass('modal_crud_w') && that.data('modal-closable')) {
        modal = that.data('modal');
    }
    
    if(modal) {
        $('.modal_crud_w[data-modal="' + modal + '"]').fadeOut(animation_speed, animation_ease, function(){
            var modal_content = $(this).find('.modal_crud');
            modal_content.find('IFRAME').each(function(){
                var iframe = $(this);
                iframe.data('src',iframe.attr('src'));
                iframe.removeAttr('src');
            });
            $('BODY').removeClass('modal_open');
        });
    }
});

$(window).resize(function(){
    resize_modal();
});

function resize_modal() {
    var modal = $('.modal_crud_w[data-modal]:visible');
    if(modal.length) {
        
        modal.removeClass('modal_long');
        var modal_content = modal.find('.modal_crud');
        
        if(modal_content.height() >= $(window).height() && !modal.hasClass('modal_long'))
            modal.addClass('modal_long');
    }
}