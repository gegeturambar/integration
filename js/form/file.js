// Custom file
$(document).ready(function(e) {
    CustomFile();
});
$(document).ajaxComplete(function(e) {
    CustomFile();
});
$(document).on('CustomForm', function(e) {
    CustomFile();
});

function CustomFile()
{
    $('INPUT[type="file"]').each(function(){
        if($(this).parent().is('LABEL.custom_file_w') === false)
        {
            $(this).wrap('<label class="custom_file_w" for="'+$(this).attr('id')+'"></label>');
            $(this).after('<span class="custom_file">'+$(this).attr('upload_text')+'</span>');
            $(this).after('<span class="icone crud-upload icone_bg"></span>');


            $(this).focus(function(e) {
                $(this).parent().addClass('focus');
            }).blur(function(e) {
                $(this).parent().removeClass('focus');
            }).change(function(e) {
                var fichier=$(this).val();
                if(fichier.indexOf('\\')!=-1)
                    fichier=fichier.substr(fichier.lastIndexOf('\\')+1);
                if(fichier!='')
                    $(this).parent().find('.custom_file').text($(this).attr('upload_text')+' : '+fichier);
                else
                    $(this).parent().find('.custom_file').text($(this).attr('upload_text'));
            });
        }
    });
}