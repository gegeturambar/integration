import removeDiacritics from '../base/clean_search.js';
import {animation_speed, animation_ease} from '../var.js';

// Custom select
$(document).ready(function (e) {
    CustomSelect();
});
$(document).ajaxComplete(function (e) {
    CustomSelect();
});
$(window).scroll(function (e) {
    CustomSelectPosition();
});
$(window).resize(function (e) {
    CustomSelectPosition();
});
$(document).on('CustomForm', function (e) {
    CustomSelect();
});

export default function CustomSelect() {

    function CustomSelectMultipleAddElement(select, element) {
        if (element.hasClass('selected')) {
            return;
        }

        var _value = element.attr('data-value');
        var _label = element.text();

        element.addClass('selected');
        var _con_tenu = element.closest('.custom_select_w').prev('.custom_select_m_list');
        _con_tenu.append('<span class="custom_select_m_list_item" data-value="' + _value + '">' + _label + '<span class="crud-close"></span></span>');
        _con_tenu.addClass('active');

        select.find('option[value="' + _value + '"]').prop('selected', true);
        select.trigger('change');
    }

    function CustomSelectMultipleRemoveElement(select, element) {
        var _value = element.attr('data-value');
        var _con_tenu = element.closest('.custom_select_m_list');

        select.find('option[value="' + _value + '"]').prop('selected', false);
        select.closest('.custom_select_w').find('.custom_option[data-value="' + _value + '"]').removeClass('selected');
        element.remove();
        if(_con_tenu.children('.custom_select_m_list_item').length == 0) {
            _con_tenu.removeClass('active');
        }
    }

    function CustomSelectMultipleRedraw(select) {
        var placeholder = select.closest('.custom_select_w').prev('.custom_select_m_list');
        placeholder.html('');
        var selected_values = select.val().length ? select.val() : [''];
        $.each(selected_values, function() {
            var custom_option = select.closest('.custom_select_w').find('.custom_option[data-value="' + this + '"]');
            CustomSelectMultipleAddElement(select, custom_option);
        });
    }

    $('SELECT[multiple]').each(function () {
        // Apply logic only once
        if ($(this).parent().is('DIV.custom_select_w')) {
            return;
        }

        //Construct
        $(this).wrap('<div class="custom_select_w multiple"></div>');
        $(this).after('<input class="custom_select_value" type="text" /><div class="custom_select_options" tabindex="-1"></div><div class="custom_select_icon"><span class="crud-bottom"></span></div>');
        $(this).closest('.custom_select_w').before('<div class="custom_select_m_list"></div>')
        $(this).closest('.custom_select_w').find('.custom_select_options').append(CustomSelectCreateOption($(this).children()));
        if ($(this).is('[autocomplete]'))
            $(this).closest('.custom_select_w').find('.custom_select_options').append('<div class="custom_select_no_result custom_select_hidden">' + $(this).attr('autocomplete') + '</div>');
        else {
            $(this).closest('.custom_select_w').find('input.custom_select_value').attr('readOnly', true);
            $(this).closest('.custom_select_w').find('input.custom_select_value').prop('readonly', true);
        }
        if ($(this).is('[disabled]'))
            $(this).closest('.custom_select_w').addClass('disabled');

        CustomSelectSearchOption('', $(this).closest('.custom_select_w').find('.custom_select_options'));
        CustomSelectMultipleRedraw($(this));

        //Click
        $(this).closest('.custom_select_w').find('.custom_select_options .custom_option').mousedown(function (e) {
            // TODO : toggle selection? Or only add item to selection?
            CustomSelectMultipleAddElement($(this).closest('.custom_select_w').find('SELECT'), $(this));
        });

        // Remove element
        $(this).closest('.custom_select_w').prev('.custom_select_m_list').on('click', '.custom_select_m_list_item .crud-close', function () {
            var element = $(this).closest('.custom_select_m_list_item');
            var select = element.closest('.custom_select_m_list').next('.custom_select_w').find('SELECT');
            CustomSelectMultipleRemoveElement(select, element);
        })

        //Error
        $(this).on('invalid',function(){
            $(this).closest('.custom_select_w').addClass('focus');
        });

        // Change / Close
        $(this).closest('.custom_select_w').find('SELECT').change(function (e) {
            // Close the drop-down
            var customSelectW = $(this).closest('.custom_select_w');
            customSelectW.removeClass('focus');
            customSelectW.find('.custom_select_options').slideUp(animation_speed, animation_ease);
        });

        //Focus / Open
        $(this).closest('.custom_select_w').find('input.custom_select_value').focus(function (e) {
            $(this).val('');
            // Open the drop-down
            var customSelectW = $(this).closest('.custom_select_w');
            customSelectW.addClass('focus');
            var con_tenu = customSelectW.find('.custom_select_options');
            con_tenu.slideDown(animation_speed, animation_ease, function() {
                con_tenu.css('display', 'block !important');
            });
        });

        //Keyup
        $(this).closest('.custom_select_w').find('input.custom_select_value').keyup(function (e) {
            if (e.which != 9 && e.which != 13 && e.which != 38 && e.which != 40) //TAB, ENTER, UP & DOWN
                CustomSelectSearchOption($(this).val(), $(this).closest('.custom_select_w').find('.custom_select_options'));
        });
    });
    $('SELECT:not([multiple])').each(function () {
        if ($(this).parent().is('DIV.custom_select_w') === false) {
            if ($(this).is('[ajax]')) {
                //Construct
                $(this).wrap('<div class="custom_select_w"></div>');
                $(this).after('<input class="custom_select_value" type="text" /><div class="custom_select_options" tabindex="-1"></div><div class="custom_select_icon"><span class="crud-bottom"></span></div>');
                RequestAjax($(this));
                CustomSelectSetValue($(this), false);

                //Click / Open
                $(this).closest('.custom_select_w').find('DIV.custom_select_options').on('mousedown', 'DIV.custom_option', function(e) {
                    var bottom = 0;
                    var customSelectW = $(this).parents().closest('.custom_select_w');
                    customSelectW.find('.custom_select_value').attr('data-value', $(this).attr('data-value'));
                    customSelectW.find('.custom_select_value').attr('placeholder', $(this).text());
                    customSelectW.find('.custom_select_value').val($(this).text());

                    if (customSelectW.find('SELECT').children('option[selected]').length > 0) {
                        customSelectW.find('SELECT').children('option[selected]').remove();
                    }

                    if (customSelectW.find('.custom_option.selected').data('value') !== $(this).attr('data-value')) {
                        var valueSecond = $(this).attr('data-value');
                        if (valueSecond !== null) {
                            var valueFirst = customSelectW.find('.custom_option.selected').data('value');
                            customSelectW.find('.custom_option.selected[data-value='+valueFirst+']').removeClass('selected');
                            customSelectW.find('.custom_option[data-value='+valueSecond+']').addClass('selected');
                        }
                    } else {
                        $(this).addClass('selected');
                    }

                    customSelectW.find('.custom_select_options').scrollTop(0);
                    if (customSelectW.find('.custom_option.selected').length) {
                        bottom += customSelectW.find('.custom_option.selected').position().top;
                        bottom += customSelectW.find('.custom_option.selected').outerHeight(true);
                    }
                    bottom -= customSelectW.find('.custom_select_options').outerHeight(true);
                    if (bottom > 0)
                        customSelectW.find('.custom_select_options').scrollTop(bottom);

                    customSelectW.find('SELECT').append('<option value="'+customSelectW.find('.custom_option.selected').data('value')+'" selected="selected"></option>');
                    customSelectW.find('SELECT').trigger('change');
                });

                //Change / Close
                $(this).closest('.custom_select_w').find('SELECT').change(function (e) {
                    var customSelectW = $(this).closest('.custom_select_w');
                    customSelectW.removeClass('focus');
                    customSelectW.find('.custom_select_options').slideUp(animation_speed, animation_ease);
                });

                //Focus
                $(this).closest('.custom_select_w').find('.custom_select_value').focus(function (e) {
                    $(this).val('');
                    var customSelectW = $(this).closest('.custom_select_w');
                    customSelectW.addClass('focus');
                    customSelectW.find('.custom_select_options').slideDown(animation_speed, animation_ease);
                });

                //Keyup
                $(this).closest('.custom_select_w').find('.custom_select_value').keyup(function (e) {
                    if (e.which != 9 && e.which != 13 && e.which != 38 && e.which != 40) //TAB, ENTER, UP & DOWN
                        CustomSelectSearchOption($(this).closest('.custom_select_w').find('.custom_select_value').val(), $(this).closest('.custom_select_w').find('.custom_select_options'));
                });
            } else {
                //Construct
                $(this).wrap('<div class="custom_select_w"></div>');
                $(this).after('<input class="custom_select_value" type="text" /><div class="custom_select_options" tabindex="-1"></div><div class="custom_select_icon"><span class="crud-bottom"></span></div>');
                $(this).closest('.custom_select_w').find('.custom_select_options').append(CustomSelectCreateOption($(this).children()));
                if ($(this).is('[autocomplete]')) {
                    $(this).closest('.custom_select_w').find('.custom_select_options').append('<div class="custom_select_no_result">' + $(this).attr('autocomplete') + '</div>');
                } else {
                    $(this).closest('.custom_select_w').find('.custom_select_value').attr('readOnly', true);
                    $(this).closest('.custom_select_w').find('.custom_select_value').prop('readonly', true);
                }

                if ($(this).is('[disabled]'))
                    $(this).closest('.custom_select_w').addClass('disabled');
                CustomSelectSetValue($(this), true);

                //Click / Open
                $(this).closest('.custom_select_w').find('.custom_select_options .custom_option').mousedown(function (e) {
                    $(this).closest('.custom_select_w').find('SELECT').val($(this).attr('data-value'));
                    $(this).closest('.custom_select_w').find('SELECT').trigger('change');
                });

                //Change / Close
                $(this).closest('.custom_select_w').find('SELECT').change(function (e) {
                    CustomSelectSetValue($(this), true);
                    var customSelectW = $(this).closest('.custom_select_w');
                    customSelectW.removeClass('focus');
                    customSelectW.find('.custom_select_options').slideUp(animation_speed, animation_ease);
                });

                //Focus
                $(this).closest('.custom_select_w').find('.custom_select_value').focus(function (e) {
                    $(this).val('');
                    var customSelectW = $(this).closest('.custom_select_w');
                    customSelectW.addClass('focus');
                    customSelectW.find('.custom_select_options').slideDown(animation_speed, animation_ease);
                });

                //Keydown
                $(this).closest('.custom_select_w').find('.custom_select_value').keydown(function (e) {
                    if (e.which == 13 || e.which == 38 || e.which == 40) //ENTER, UP & DOWN
                    {
                        if (e.which == 13) {
                            e.preventDefault();
                            $('.custom_select_w.focus').find('SELECT').trigger('change');
                        }
                        else
                            CustomSelectFindOption($(this).closest('.custom_select_w').find('.custom_option.selected'), e.which, true);
                    }
                });

                //Keyup
                $(this).closest('.custom_select_w').find('.custom_select_value').keyup(function (e) {
                    if (e.which != 9 && e.which != 13 && e.which != 38 && e.which != 40) //TAB, ENTER, UP & DOWN
                        CustomSelectSearchOption($(this).closest('.custom_select_w').find('.custom_select_value').val(), $(this).closest('.custom_select_w').find('.custom_select_options'));
                });
            }
        }
    });
    CustomSelectPosition();
}

//Blur
$(document).click(function (e) {
    if (!$(e.target).closest('.custom_select_w.focus').length) {
        $('.custom_select_w.focus').find('SELECT').trigger('change');
    } else  {
        $('.custom_select_w.focus').find('SELECT').each(function () {
            if ($(this).attr('id') !== $(e.target).closest('.custom_select_w.focus').find('SELECT').attr('id')) {
                $(this).trigger('change');
            }
        })
    }
});

function RequestAjax(element) {
    var input = $(element).closest('.custom_select_w').find('input.custom_select_value');
    var placeholder = $(element).closest('.custom_select_w').find('SELECT').attr('placeholder');
    input.on('keyup', function (e) {
        if (e.which != 9 && e.which != 13 && e.which != 38 && e.which != 40) {
            var term = $(input).val();
            if (term.length > 2) {
                $.ajax({
                    url: $(element).attr('ajax'),
                    method: 'post',
                    dataType: 'json',
                    global: true,
                    data: {
                        term: term,
                    }
                }).done(function (response) {
                    if (Object.keys(response).length > 0) {
                        $(element).closest('.custom_select_w').find('.custom_select_options').empty().append(CustomSelectAjaxCreateOption(response, placeholder));
                    } else {
                        $(element).closest('.custom_select_w').find('.custom_select_options').empty().append('<div class="custom_select_no_result"></div>');
                    }
                }).fail(function (jqXHR, textStatus) {
                    console.log("Request failed: " + textStatus);
                });
            }
        }
    });
}

function CustomSelectAjaxCreateOption(response, placeholder) {
    var options = '';
    options += '<div class="custom_option" data-value="0">' + placeholder + '</div>';
    $.each(response,  function( key, value ) {
        options += '<div class="custom_option" data-value="' + value + '">' + key.trim() + '</div>';
    });
    return options;
}

function CustomSelectCreateOption(children) {
    var options = '';
    children.each(function () {
        if ($(this).is('OPTGROUP')) {
            options += '<div class="custom_optgroup">';
            if ($(this).is('[label]'))
                options += '<div class="custom_optgroup_label">' + $(this).attr('label').trim() + '</div>';
            options += CustomSelectCreateOption($(this).children());
            options += '</div>';
        }
        else
            options += '<div class="custom_option" data-value="' + $(this).attr('value') + '">' + $(this).text().trim() + '</div>';
    });

    return options;
}

function CustomSelectSetValue(element, validate) {
    if (validate)
        CustomSelectSearchOption('', element.closest('.custom_select_w').find('.custom_select_options'));

    var option_value = element.closest('.custom_select_w').find('SELECT').val();
    element.closest('.custom_select_w').find('.custom_select_value').attr('data-value', option_value);

    var option_texte = element.closest('.custom_select_w').find('SELECT OPTION[value="' + option_value + '"]').text().trim();
    if (validate)
        element.closest('.custom_select_w').find('.custom_select_value').val(option_texte);
    element.closest('.custom_select_w').find('.custom_select_value').attr('placeholder', option_texte);

    element.closest('.custom_select_w').find('.custom_option').removeClass('selected');
    element.closest('.custom_select_w').find('.custom_option[data-value="' + option_value + '"]').addClass('selected');

    element.closest('.custom_select_w').find('.custom_select_options').scrollTop(0);
    var bottom = 0;
    if (element.closest('.custom_select_w').find('.custom_option.selected').length) {
        bottom += element.closest('.custom_select_w').find('.custom_option.selected').position().top;
        bottom += element.closest('.custom_select_w').find('.custom_option.selected').outerHeight(true);
    }
    bottom -= element.closest('.custom_select_w').find('.custom_select_options').outerHeight(true);
    if (bottom > 0)
        element.closest('.custom_select_w').find('.custom_select_options').scrollTop(bottom);
}

function CustomSelectPosition() {
    $('.custom_select_w').each(function () {

        var bottom_element = $(this).offset().top + $(this).outerHeight(true) + parseFloat($(this).find('.custom_select_options').css('max-height')) + 10;
        var bottom_window = $(window).scrollTop() + $(window).outerHeight(true);

        if (bottom_element > bottom_window)
            $(this).addClass('custom_select_top');
        else
            $(this).removeClass('custom_select_top');
    });
}

function CustomSelectFindOption(element, way) {
    var sub_element;
    if (element.is('.custom_select_options')) {
        if (element.find('.custom_option:not(.custom_select_hidden):first').length) {
            element.closest('.custom_select_w').find('SELECT').val(element.find('.custom_option:not(.custom_select_hidden):first').attr('data-value'));
            CustomSelectSetValue(element, false);
        }
    }
    else {
        if (element.is('.custom_optgroup')) {
            if (way == 38)
                sub_element = element.find(':not(.custom_optgroup_label):not(.custom_select_hidden):last');
            else if (way == 40)
                sub_element = element.find(':not(.custom_optgroup_label):not(.custom_select_hidden):first');
        }
        else {
            if (way == 38)
                sub_element = element.prevAll(':not(.custom_optgroup_label):not(.custom_select_hidden)').first();
            else if (way == 40)
                sub_element = element.nextAll(':not(.custom_optgroup_label):not(.custom_select_hidden)').first();
        }

        if (!sub_element.length) {
            sub_element = element.parent();

            if (sub_element.is('.custom_optgroup')) {
                if (way == 38)
                    var ter_element = sub_element.prevAll(':not(.custom_optgroup_label):not(.custom_select_hidden)').first();
                else if (way == 40)
                    var ter_element = sub_element.nextAll(':not(.custom_optgroup_label):not(.custom_select_hidden)').first();

                if (ter_element.is('.custom_option')) {
                    ter_element.closest('.custom_select_w').find('SELECT').val(ter_element.attr('data-value'));
                    CustomSelectSetValue(ter_element, false);
                }
                else if (sub_element.is('.custom_optgroup'))
                    CustomSelectFindOption(ter_element, way);
            }
        }

        else if (sub_element.is('.custom_option')) {
            sub_element.closest('.custom_select_w').find('SELECT').val(sub_element.attr('data-value'));
            CustomSelectSetValue(sub_element, false);
        }

        else if (sub_element.is('.custom_optgroup'))
            CustomSelectFindOption(sub_element, way);
    }
}

function CustomSelectSearchOption(search, element) {
    if (element.is('.custom_select_options')) {
        element.find('.custom_select_no_result').addClass('custom_select_hidden');
        search = removeDiacritics(search);
    }

    element.children().each(function () {
        if ($(this).is('.custom_option')) {
            if (!removeDiacritics($(this).text()).match(new RegExp(search.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"), 'i')))
                $(this).addClass('custom_select_hidden');
            else
                $(this).removeClass('custom_select_hidden');
        }
        else if ($(this).is('.custom_optgroup')) {
            CustomSelectSearchOption(search, $(this));
            if (!$(this).children(':not(.custom_optgroup_label):not(.custom_select_hidden)').length)
                $(this).addClass('custom_select_hidden');
            else
                $(this).removeClass('custom_select_hidden');
        }
    });

    if (element.is('.custom_select_options') && !element.children(':not(.custom_select_hidden)').length)
        element.find('.custom_select_no_result').removeClass('custom_select_hidden');
}