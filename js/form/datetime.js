var vanillaTextMask = require('vanilla-text-mask');
require('jquery-caret');

// Custom datetime
$(document).ready(function(e) {
    CustomDateTime();
});
$(document).ajaxComplete(function(e) {
    CustomDateTime();
});
$(document).on('CustomForm', function(e) {
    CustomDateTime();
});
$(window).resize(function(e) {
    CustomDateTimeResize();
});

function CustomDateTime() {
    $('input[type=date]').each(function () {
        var maskElement = $('#text_mask_' + $(this).attr('id'));
        if ($(this).val().length != 0 ) {
            maskElement.val(nativeWidgetToTextMaskFormat($(this).val(), $(this).attr('type'), maskElement.data('pattern')));
        }
        textMask($(this));
    });

    $('input[type=time]').each(function () {
        var maskElement = $('#text_mask_' + $(this).attr('id'));
        if ($(this).val().length != 0 ) {
            maskElement.val(nativeWidgetToTextMaskFormat($(this).val(), $(this).attr('type'), maskElement.data('pattern')));
        }
        textMask($(this));
    });

    CustomDateTimeResize();
}

function CustomDateTimeResize() {
    $('input[type=date]').each(function () {
        var row = $(this).closest('.form_row');
        var desktop = row.find('.date_desktop INPUT');
        var mobile = row.find('.date_mobile INPUT');
        if((desktop.prop('required') && mobile.is(':visible')) || (mobile.prop('required') && desktop.is(':visible'))) {
            desktop.prop('required',!desktop.prop('required'));
            mobile.prop('required',!mobile.prop('required'));
        }
    });
    $('input[type=time]').each(function () {
        var row = $(this).closest('.form_row');
        var desktop = row.find('.time_desktop INPUT');
        var mobile = row.find('.time_mobile INPUT');
        if((desktop.prop('required') && mobile.is(':visible')) || (mobile.prop('required') && desktop.is(':visible'))) {
            desktop.prop('required',!desktop.prop('required'));
            mobile.prop('required',!mobile.prop('required'));
        }
    });
}

/**
 * Check if date string is valid date.
 */
function isDateValid(date) {
    return false === isNaN(Date.parse(date));
}

/**
 * Check if time string is valid time, not like (29:00) is not valid
 */
function isTimeValid(time) {
    return /^([0-1_][0-9_]|[2_][0-3_]):([0-5_][0-9_])$/.test(time);
}

/**
 * Compute pattern to valid mask (replace regex by real javascript regex)
 */
function patternToMask(pattern) {
    var mask = pattern.split(',');

    for (var i = 0, len = mask.length; i < len; i++) {
        if (-1 !== mask[i].indexOf('regex')) {
            mask[i] = new RegExp(mask[i].replace('regex', ''));
        } else {
            mask[i] = mask[i]; // TODO why I have to set again value ?
        }
    }
    return mask;
}

/**
 * Compute text mask to native widget valid format
 * Return false if is not valid
 */
function textMaskToNativeWidgetFormat(value, type, pattern){
    var date, time, yPosition, mPosition, dPosition, hPosition;
    var returnValue = false;

    if ('date' === type && undefined !== pattern) {
        date = 'yyyy-MM-dd';
        yPosition = pattern.indexOf('yyyy');
        mPosition = pattern.indexOf('MM');
        dPosition = pattern.indexOf('dd');

        date = date
            .replace('yyyy', value.substring(yPosition, yPosition + 4))
            .replace('MM', value.substring(mPosition, mPosition + 2))
            .replace('dd', value.substring(dPosition, dPosition + 2));

        if (isDateValid(date)) { returnValue = date; }

    } else if ('time' === type && undefined !== pattern) {
        time = 'HH:mm';
        hPosition = pattern.indexOf('HH');
        mPosition = pattern.indexOf('mm');

        time = time
            .replace('HH', value.substring(hPosition, hPosition + 2))
            .replace('mm', value.substring(mPosition, mPosition + 2));

        if (isTimeValid(time)) { returnValue = time; }
    }

    return returnValue;
}

/**
 * Compute native widget format to text mask
 */
function nativeWidgetToTextMaskFormat(value, type, pattern){
    var returnValue;

    if ('date' === type) {
        returnValue = pattern
            .replace('yyyy', value.substring(0, 4))
            .replace('MM', value.substring(5, 7))
            .replace('dd', value.substring(8, 10));

    } else if ('time' === type) {
        returnValue = pattern
            .replace('HH', value.substring(0, 2))
            .replace('mm', value.substring(3, 5));
    }

    return returnValue;
}

/**
 *  Remove last char if is a number
 * @param element
 * @param cursorPosition
 */
function removeLastChar(element, cursorPosition)
{
    cursorPosition = cursorPosition || element.caret();
    if (isNaN(element.val().substring(cursorPosition, cursorPosition - 1))) {
        cursorPosition = cursorPosition - 1;
    }
    element.val(element.val().replace(element.val().substring(cursorPosition, cursorPosition - 1), '_'));

    return cursorPosition
}

/**
 * Declare textMask
 * @param element
 */
function textMask(element) {
    var id, maskElementId, maskElement, validDateOrTime, cursorPosition;

    maskElementId = '#text_mask_' + element.attr('id');
    maskElement = $(maskElementId);

    maskElement.on('keyup', function (e) {
        if (0 !== $(this).val().length) {
            if (-1 === $(this).val().indexOf('_')) {
                if (false !== (validDateOrTime = textMaskToNativeWidgetFormat($(this).val(), element.attr('type'), $(this).data('formattedPattern')))) {
                    element.val(validDateOrTime);
                    element.trigger('change');
                }
            } else if ('time' === element.attr('type') && ! isTimeValid($(this).val())) {
                $(this).caret(removeLastChar($(this)) - 1); // Remove last char if time is not valid like (29:__) ==> (2_:__)
            } else if ('date' === element.attr('type')) {
                cursorPosition = $(this).caret();
                if ('/' === $(this).val().substring(cursorPosition, cursorPosition - 1)) {
                    cursorPosition = cursorPosition - 1;
                    if ('0' === $(this).val().substring(cursorPosition, cursorPosition - 1) && '0' === $(this).val().substring(cursorPosition - 1, cursorPosition - 2)) {
                        cursorPosition = removeLastChar($(this)); // Remove double 0before/
                        $(this).caret(removeLastChar($(this), cursorPosition)-2); // Remove double 0before/
                    }
                }
            }

        } else {
            element.val('');
        }
    });

    maskElement.on('blur', function (e) {
        if (false === (validDateOrTime = textMaskToNativeWidgetFormat($(this).val(), element.attr('type'), $(this).data('formattedPattern')))) {
            maskElement.val('');
        } else {
            maskElement.trigger('keyup');
        }
        e.preventDefault();
    });

    element.on('blur', function (e) {
        if (0 !== element.val().length) {
            maskElement.val(nativeWidgetToTextMaskFormat($(this).val(), $(this).attr('type'), maskElement.data('pattern')));
            maskElement.trigger('change');
            e.preventDefault();
        }
    });

    if (undefined !== maskElement.data('textMaskPattern')) {
        vanillaTextMask.maskInput({
            inputElement: document.querySelector(maskElementId),
            mask: patternToMask(maskElement.data('textMaskPattern')),
            keepCharPositions: true,
        });
    }
}