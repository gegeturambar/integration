$(document).ready(function collection() {
    collectionAdd();
    collectionRemove();
    collectionHideAddButton();
    collectionComputeLabel();
});

function collectionAdd () {
    $('form').on('click', '.collection-add', function (e) {
        e.preventDefault();
        var parent = $(this).parent();
        var prototypeHtml = parent.attr('data-prototype');
        var prototypeName = parent.attr('data-prototype-name');

        // On remplace le name par le numéro de l'item dans la collection
        prototypeHtml = prototypeHtml.replace(new RegExp(prototypeName,'g'), parent.children().length - 1);
        prototypeHtml = prototypeHtml.replace(new RegExp(prototypeName + 'label__','g'),  parent.children().length - 1);

        // On insère le prototype avant le bouton d'ajout

        $(prototypeHtml).insertBefore(this);
        
        eval($(prototypeHtml).find('SCRIPT').text());
        $(document).trigger('CustomForm');

        collectionComputeLabel();
        collectionShowAddButton();
        collectionHideAddButton();
    });
}

function collectionRemove () {
    $('form').on('click', '.collection-delete', function (e) {
        e.preventDefault();

        // On remonte au parent avec la classe collection-row et on le supprime
        $(this).parent().closest('.collection-row').remove()
        collectionShowAddButton();
        collectionComputeLabel();
    });
}

function collectionHideAddButton() {
    var collectionRows = $('.collection-rows'),
        protoypeLimit = collectionRows.data('prototypeLimit'),
        collectionCount = collectionRows.children().length
    ;
    if (undefined !== protoypeLimit && 0 !== protoypeLimit && collectionCount > 0 && (collectionCount - 1) >= protoypeLimit) {
        collectionRows.find('.collection-add').hide();
    }
}

function collectionShowAddButton() {
    $('.collection-rows').find('.collection-add').show();
}

function collectionComputeLabel() {
    $('.collection-rows').find('.collection-row').each(function (index) {
        replaceData($(this), index+1)
    });
}

function replaceData(element, i) {
    var parent = element.closest('.collection-rows'), name, fullName, label, id, entryTypeRow, entryTypeLabel, entryTypeForm;

    name = parent.data('prototypeName');
    if(parent.data('prototypeFull_name') !== undefined)
        fullName = parent.data('prototypeFull_name').replace(name, i);
    if(parent.data('prototypeLabel') !== undefined)
        label = parent.data('prototypeLabel').replace(name, i);
    if(parent.data('prototypeId') !== undefined)
        id = parent.data('prototypeId').replace(name, i);

    element.attr('name', fullName);

    entryTypeRow = element.find('.form_row');

    if (entryTypeRow !== null) {
        entryTypeLabel = $('label', entryTypeRow).first();
        if (entryTypeLabel !== null) {
            entryTypeLabel.attr('for', id);
            entryTypeLabel.html(label);
        }

        entryTypeForm = entryTypeRow.find('#' + entryTypeLabel.attr('for'));
        if (entryTypeForm !== null) {
            entryTypeForm.attr('name', fullName);
            entryTypeForm.attr('id', id);
        }
    }
}