var vanillaTextMask = require('vanilla-text-mask');

/**
 * Compute pattern to valid mask (replace regex by real javascript regex)
 */
function patternToMask(pattern) {
    var mask = pattern.split(',');

    for (var i = 0, len = mask.length; i < len; i++) {
        if (-1 !== mask[i].indexOf('regex')) {
            mask[i] = new RegExp(mask[i].replace('regex', ''));
        } else {
            mask[i] = mask[i]; // TODO why I have to set again value ?
        }
    }
    return mask;
}

$(function () {
    $('input[data-text-mask-pattern]').each(function () {
        if ($(this).attr('id') !== undefined && $(this).attr('id') !== '') {
           vanillaTextMask.maskInput({
               inputElement: document.querySelector('#' + $(this).attr('id')),
               mask: patternToMask($(this).data('textMaskPattern')),
               keepCharPositions: true,
               placeholderChar: undefined === $(this).data('textMaskPlaceholderChar') ? '_' : $(this).data('textMaskPlaceholderChar'),
               guide: false !== $(this).data('textMaskGuide')
           });
        } else {
            console.warn('Missing id on element with data-text-mask-pattern attribute');
        }
    });
});