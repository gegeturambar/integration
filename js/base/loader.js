import {animation_speed, animation_ease} from '../var.js';

export function ToggleLoader() {
    $('.loading').fadeToggle(animation_speed, animation_ease, function() {
        let that = $(this);
        var body = $('HTML, BODY');

        body.css('overflow','none' != that.css('display') ? 'hidden' : '');
        body.css('-webkit-overflow-scrolling','none' != that.css('display') ? 'auto' : '');
    });
}