$(document).ready(function(){
    FixHeight();
});

$(window).resize(function(){
    FixHeight();
});

export function FixHeight() {

    var containers = $('[data-resize="container"]');

    if(containers.length) {
        containers.each(function(){

            var container = $(this);
            var elements = container.find('[data-resize="element"]');

            if(elements.length) {

                elements.css('height', '');
                if(container.find('[data-resize="active"]:visible') && container.find('[data-resize="active"]').css('display') != 'none') {

                    var max_height = new Array();

                    elements.each(function () {
                        var element = $(this);
                        var height = element.outerHeight(false);
                        var group = element.data('resize-group') ? element.data('resize-group') : '';
                        if(max_height[group] === undefined)
                            max_height[group] = 0;

                        if (height > max_height[group])
                            max_height[group] = height;
                    });
                    for (var group in max_height){
                        container.find('[data-resize="element"][data-resize-group="' + group + '"]').css('height', max_height[group]);
                    }
                    container.find('[data-resize="element"]:not([data-resize-group])').css('height', max_height['']);
                }
            }
        });
    }
}