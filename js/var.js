export var animation_speed = 400, animation_ease = 'linear', animation_transition = false;

export function setAnimationSpeed(value) {
    animation_speed = value;
}

export function setAnimationEase(value) {
    animation_ease = value;
}

export function setAnimationTransition(value) {
    animation_transition = value;
}