import {animation_speed, animation_ease} from '../var.js';

//Passer le menu en mode barre
$(document).on('click', '.menu_trigger', function() {
    var menu_wrapper = $(this).closest('.menu_w');
    menu_wrapper.toggleClass('light');

    var date = new Date();
    date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));
    document.cookie='menu_light=' + (menu_wrapper.hasClass('light') === true ? 'true' : 'false') + '; expires='+date.toGMTString()+'; path=/';
});

//Ouvrir le menu mobile
$(document).on('click', '.menu_trigger_head', function() {
    $('html,body').animate({scrollTop:0},animation_speed, animation_ease,function(){
        $('.menu_w').animate({
            left: "0%"
        }, animation_speed, animation_ease, function(){
            $('.menu_w').css('left','');
            $('.menu_w').addClass('open');
            $('HTML,BODY').css('overflow','hidden');
            $('HTML,BODY').css('-webkit-overflow-scrolling','auto');
        });
    });
});

//Fermer le menu mobile
$(document).on('click', '.menu_close .icone', function() {
    $('.menu_w').animate({
        left: "-100%"
    }, animation_speed, animation_ease, function(){
        $('.menu_w').css('left','');
        $('.menu_w').removeClass('open');
        $('HTML,BODY').css('overflow','');
        $('HTML,BODY').css('-webkit-overflow-scrolling','');
    });
});

//Ouvrir les sous menus
$(document).on('click', '.menu_w DIV.menu_item', function() {

    var menu = $(this).closest('.menu_item_w');
    var class_open = $(this).closest('.menu_w').hasClass('light') ? 'light_open' : 'open';

    if (!menu.hasClass(class_open)) {
        $('.menu_item_w.' + class_open).find('.submenu').slideToggle(animation_speed, animation_ease, function(){
            $(this).closest('.menu_item_w').toggleClass(class_open);
            $(this).css('display','');
        });
    }

    menu.find('.submenu').slideToggle(animation_speed, animation_ease, function(){
        $(this).closest('.menu_item_w').toggleClass(class_open);
        $(this).css('display','');
    });
});