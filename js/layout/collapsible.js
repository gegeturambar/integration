document.addEventListener("click", function(e) {
    var trigger = e.target;
    while (trigger.classList && !trigger.classList.contains("js-collapsible-trigger")) {
        trigger = trigger.parentNode;
        if (!trigger || trigger.nodeType === 9) {
            return;
        }
    }
    if (trigger.tagName === 'A') {
        if (e.ctrlKey || e.button === 4) {
            return;
        }
        e.preventDefault();
    }
    var collapsibles = [],
        controls = trigger.getAttribute("aria-controls");

    if (controls) {
        for (var i = 0, ids = controls.split(" "), id ; id = ids[i] ; ++i) {
            var controlled = document.getElementById(id);
            if (controlled && controlled.classList.contains("collapsible")) {
                collapsibles.push(controlled);
            }
        }
    } else {
        var parent = trigger;
        while (parent.classList && !parent.classList.contains("js-collapsible-parent")) {
            parent = parent.parentNode;
        }
        if (trigger.nodeType === 1) {
            collapsibles = parent.querySelectorAll(".collapsible");
        }
    }

    for (var i = 0, collapsible ; collapsible = collapsibles[i] ; ++i) {
        if (collapsible.getAttribute("aria-expanded") === "false") {
            expand(collapsible);
        } else {
            collapse(collapsible);
        }
    }
});

function collapse(collapsible)
{
    var height = collapsible.scrollHeight;

    collapsible.style.transitionDuration = (height * (seekAttribute(collapsible, "data-collapse-speed-ratio") || 2)) + "ms";
    collapsible.style.maxHeight = height + "px";

    getComputedStyle(collapsible).getPropertyValue("max-height"); // flush style for transition

    collapsible.setAttribute("aria-expanded", "false");
    collapsible.removeAttribute("style");
}

function expand(collapsible)
{
    var group = seekAttribute(collapsible, "data-collapsible-group");
    if (group) {
        var groupCollapsibles = document.querySelectorAll(
            "[data-collapsible-group='" + group + "']:not([aria-expanded=false])," +
            "[data-collapsible-group='" + group + "'] .collapsible:not([aria-expanded=false])"
        );
        for (var i = 0,  groupCollapsible ; groupCollapsible = groupCollapsibles[i] ; ++i) {
            collapse(groupCollapsible);
        }
    }

    var height = collapsible.scrollHeight,
        transitionDuration = height * (seekAttribute(collapsible, "data-collapse-speed-ratio") || 2);

    collapsible.style.transitionDuration = transitionDuration + "ms";
    collapsible.style.maxHeight = height + "px";
    collapsible.setAttribute("aria-expanded", "true");
    setTimeout(function() {
        collapsible.style.maxHeight = "none";
    }, transitionDuration);
}

function seekAttribute(collapsible, attribute)
{
    if (collapsible.hasAttribute(attribute)) {
        return collapsible.getAttribute(attribute);
    }

    var parent = collapsible.parentNode;

    while (!parent.hasAttribute(attribute)) {
        parent = parent.parentNode;
        if (parent.nodeType === 9) {
            return;
        }
    }

    var attributeValue = parent.getAttribute(attribute);

    collapsible.setAttribute(attribute, attributeValue);

    return attributeValue;
}