//Ouvrir le menu utilisateur
$(document).on('click', '.header .user_trigger', function() {
    $(this).closest('.header').find('.user_dropdown').slideToggle(animation_speed, animation_ease);
});

//Déconnexion
$(document).ready(function(e) {
    var logout = $("#modal_crud_logout");

    $('.header').on('click', '.logout', function (event) {
        event.preventDefault();
        if (logout) {
            logout.show();
            logout.find('.error_button').attr('href', $(this).attr('href'));
        }
    });

    logout.on('click', '.error_button', function () {
        $(this).css('pointer-events', 'none');
    });

    logout.on('click', '.grey_button, .crud-close', function () {
        logout.hide();
    });
});