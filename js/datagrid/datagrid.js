import {animation_speed, animation_ease} from '../var.js';

var debounce = require("debounce");
var historyActive = false;
var nbElementResponsiveDatagrid = 5;

export function setNbElementResponsiveDatagrid(value) {
    nbElementResponsiveDatagrid = value;
}

$(document).ready(function(e) {
    CrudDatagridNbElements();
    CrudFilterAjax();
    CrudDatagridAjax();
    CrudDatagridEditable();
    CrudDatagridActionDelete();
    CrudDatagridSortableInputable();
    CrudDatagridPaginator();
});

function CrudDatagridNbElements()
{
    var datagridW = $('.datagrid_w');
    datagridW.on('change', 'select[name="max_per_page"]', function(event) {
        var that = $(this);
        var href= that.attr('href');
        var formData = that.serialize();
        $.ajax({
            url: href,
            method: 'get',
            data: formData
        }).done(function (data) {
            updateUrl(this.url);
            updateDatagrid(datagridW, data);
        });
    });
}

//Ouverture/Fermeture des champs en mobile
$(document).on('click', '.datagrid .datagrid_content .datagrid_row .trigger_data_row', function() {
    var datagrid_button = $(this);
    var datagrid_row = datagrid_button.closest('.datagrid_row');
    
    if(datagrid_row.hasClass('row_open')) {
        datagrid_row.find('.datagrid_row_mobile').slideUp(animation_speed, animation_ease, function(){
            datagrid_row.removeClass('row_open');
            datagrid_button.find('.icone').removeClass('crud-minus').addClass('crud-plus');
        });
    } else {
        datagrid_row.addClass('row_open');
        datagrid_row.find('.datagrid_row_mobile').slideDown(animation_speed, animation_ease);
        datagrid_button.find('.icone').removeClass('crud-plus').addClass('crud-minus');
    }
})
$(document).on('click', '.datagrid .datagrid_content .datagrid_row .mobile_main A', function(event) {
    var trigger_button = $(this).closest('.datagrid_row').find('.trigger_data_row');
    if(trigger_button.length && trigger_button.is(':visible')) {
        event.preventDefault();
        trigger_button.trigger('click');
    }
});

//Ouverture/Fermeture des actions des élements
$(document).on('click', '.datagrid .datagrid_content .datagrid_row .action_cell .action_button', function() {

    if(!$(this).closest('.action_cell').find('.tooltip_arrow').hasClass('open') && $(this).closest('.datagrid_content').find('.tooltip_arrow.open').length)
        $(this).closest('.datagrid_content').find('.tooltip_arrow.open').closest('.action_cell').find('.action_button').trigger('click');

    $(this).closest('.action_cell').find('.tooltip_arrow, .action_tooltip').fadeToggle(animation_speed/2, animation_ease,function(){
        if($(this).is('.tooltip_arrow'))
            $(this).toggleClass('open');
        $(this).css('display','');
    });
});

function CrudFilterAjax() {
    var form = $('.datagrid_top .filter form, .datagrid_top .search form');
    form.on('keyup', 'input[type="text"]', debounce(ajaxFilter, 350));
    form.on('change', 'select', ajaxFilter);
}

function CrudDatagridAjax() {
    var datagridW = $('.datagrid_w');
    datagridW.on('click', '.datagrid_head_item a, .datagrid_pagination a, .ajaxable', function(event){
        event.preventDefault();
        var that = $(this);
        var url = that.data('url') || that.attr('href');
        var urlData = that.hasClass('ajaxable') ? url : url.match(/(\?.*)/)[1];
        if (urlData) {
            urlData = urlData.substr(1);
            $.ajax({
                url: url,
                method: 'GET'
            }).done(function (data) {
                if (that.data('updateUrl') || that.data('updateUrl') === undefined) {
                    updateUrl(this.url);
                }
                updateDatagrid(datagridW, data);
            });
        }
    });
}

function CrudDatagridPaginator()
{
    var datagridW = $('.datagrid_w');
    datagridW.on('change', '.datagrid_bottom select[name="page"]', function(event) {
        console.log('in');
        var that = $(this);
        $.ajax({
            url: that.val(),
            method: 'get'
        }).done(function (data) {
            updateUrl(this.url);
            updateDatagrid(datagridW, data);
        });
    });
}

function ajaxFilter(event) {
    var datagridW = $('.datagrid_w');
    var $form = $(event.delegateTarget);
    var formData = $form.serialize();

    $.ajax({
        url: $form.attr('action'),
        method: $form.attr('method'),
        data: formData
    }).done(function (data) {
        updateUrl(this.url);
        updateDatagrid(datagridW, data);
    });
}

function CrudDatagridEditable() {
    var $datagridContent = $('.datagrid .datagrid_content');
    var datagridRowItem = '.datagrid_row_item';

    $datagridContent.on('dblclick', '.x-editable', function(){
        var that = $(this);
        var inputValue = $('<input>').attr({value: that.attr('data-value'), name: 'value'});
        var inputPk = $('<input>').attr({type: 'hidden', value: that.attr('data-pk'), name: 'pk'});
        var form = $('<form>').addClass(that.attr('class')).attr('action', that.attr('data-url')).append(inputValue).append(inputPk);
        that.hide();
        that.closest(datagridRowItem).append(form);
    });

    $datagridContent.on('submit', 'form.x-editable', function(event){
        event.preventDefault();
        var form = $(this);
        $.ajax({
            method: 'post',
            url: form.attr('action'),
            data: form.serialize()
        }).done(function(data){
            form.closest(datagridRowItem).html(data);
        });
    });
}

function CrudDatagridActionDelete() {
    var confirm = $("#modal_crud_confirm");

    $('.datagrid_w').on('click', 'a.crud-trash', function(event) {
        event.preventDefault();
        if (confirm) {
            confirm.show();
            confirm.find('.error_button').attr('href', $(this).attr('href'));
        }
    });

    confirm.on('click', '.error_button', function() {
        $(this).css('pointer-events','none');
    });

    confirm.on('click', '.grey_button, .crud-close', function() {
        confirm.hide();
    });
}

function CrudDatagridSortableInputable() {
    var datagridW = $('.datagrid_w')
    datagridW.on('change', '.order_cell INPUT, .inputable_cell INPUT', function(event) {
        var form = $(this).closest('form');
        if(!form.data('already-submit') && 'undefined' === typeof datagridW.attr('data-no-ajax-submit')) {
            form.find('.order_cell INPUT, .inputable_cell INPUT').attr('readonly',true);
            form.find('.order_cell INPUT, .inputable_cell INPUT').addClass('disabled');
            form.data('already-submit','true');
            form.submit();
        }
    });
}

function updateDatagrid(datagrid, data) {
    datagrid.find('.datagrid_bottom').remove();
    datagrid.find('.datagrid').replaceWith(data);
}

function updateUrl(url) {
    if (window.history && window.history.pushState && url) {
        if (!historyActive) {
            historyActive = true;
            var originUrl = window.document.location.href;
            window.history.pushState({href: originUrl}, null, originUrl);
        }
        window.history.pushState({href: url}, null, url);
    }
}

window.addEventListener('popstate', function(e){
    var datagridW = $('.datagrid_w');
    if (e.state && datagridW.length) {

        var href = e.state.href;
        $.ajax({
            url: href,
            method: 'get'
        }).done(function (data) {
            updateDatagrid(datagridW, data);
        });
    }
});