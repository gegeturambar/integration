import {animation_speed, animation_ease, animation_transition, setAnimationTransition} from '../var.js';

// Ouverture/Fermeture du moteur de recherche
$(document).on('click', '.crud_toolbar .crud-search', function() {
    if(!animation_transition) {
        setAnimationTransition(true);

        var datagrid = $(this).closest('.crud_toolbar').parent().find('.datagrid_w');

        if(datagrid.find('.datagrid_top .filter').hasClass('open')) {

            setAnimationTransition(false);

            datagrid.parent().find('.crud_toolbar .crud-filter').trigger('click');

            setTimeout(function(){
                datagrid.parent().find('.crud_toolbar .crud-search').trigger('click');
            },animation_speed + 10);

        } else {

            datagrid.find('.datagrid_top .search').slideToggle(animation_speed, animation_ease,function(){
                $(this).toggleClass('open');
                setAnimationTransition(false);
            });

        }
    }
});

/* open search when class open */
$(document).ready(function(){
    if($('.datagrid_w .datagrid_top .search').hasClass('open')){
        $('.datagrid_w .datagrid_top .search').removeClass('open');
        $('.crud_toolbar .crud-search').trigger('click');
    }
});

// Ouverture/Fermeture des filtres
$(document).on('click', '.crud_toolbar .crud-filter', function() {
    if(!animation_transition) {
        setAnimationTransition(true);

        var datagrid = $(this).closest('.crud_toolbar').parent().find('.datagrid_w');

        if(datagrid.find('.datagrid_top .search').hasClass('open')) {

            setAnimationTransition(false);

            datagrid.parent().find('.crud_toolbar .crud-search').trigger('click');

            setTimeout(function(){
                datagrid.parent().find('.crud_toolbar .crud-filter').trigger('click');
            },animation_speed + 10);

        } else {

            datagrid.find('.datagrid_top .filter').slideToggle(animation_speed, animation_ease,function(){
                $(this).toggleClass('open');
                setAnimationTransition(false);
            });

        }
    }
});

/* open filter when class open */
$(document).ready(function(){
    if($('.datagrid_w .datagrid_top .filter').hasClass('open')){
        $('.datagrid_w .datagrid_top .filter').removeClass('open');
        $('.crud_toolbar .crud-filter').trigger('click');
    }
});

// Ouverture/Fermeture des actions de la page
$(document).on('click', '.crud_toolbar .action_button', function() {
    $(this).find('.action_button_dropdown').slideToggle(animation_speed, animation_ease);
    $(this).toggleClass('opened');
});
$(document).click(function(e){
    if(!$(e.target).closest('.action_button.opened').length) {
        $('.crud_toolbar .action_button.opened').trigger('click');
    }
});

// reset all filter
$(document).on('click', '.crud_toolbar .resetAllFilter', function() {
    window.history.pushState({}, null, window.location.pathname);
    window.location = window.location;
});
