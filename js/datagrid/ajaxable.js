import {animation_speed, animation_ease} from '../var.js';

$(document).ready(function(e) {
    ajaxable();
});

function ajaxable() {
    var ajaxable = $('.ajaxable-with-target');

    ajaxable.on('click', function(event){
        event.preventDefault();
        var that = $(this);
        var url = that.data('url') || that.attr('href');
        if (url) {
            $.ajax({
                url: url,
                method: 'GET'
            }).done(function (data) {
                if (that.data('updateUrl') || that.data('updateUrl') === undefined) {
                    var url = this.url;
                    if (window.history && window.history.pushState && url) {
                        if (!historyActive) {
                            historyActive = true;
                            var originUrl = window.document.location.href;
                            window.history.pushState({href: originUrl}, null, originUrl);
                        }
                        window.history.pushState({href: url}, null, url);
                    }
                }
                var target = $(that.data('target'));
                if (target) {
                    target.html(data);
                }
            });
        }
    });
}